#ifndef STREAM9_MYERS_DIFF_BOX_HPP
#define STREAM9_MYERS_DIFF_BOX_HPP

#include "point.hpp"

#include <cassert>
#include <iostream>

namespace stream9::myers_diff {

class box_t
{
public:
    box_t(point_t const top_left, point_t const bottom_right)
        : m_top_left { top_left }
        , m_bottom_right { bottom_right }
    {
        assert(m_bottom_right.x() >= m_top_left.x());
        assert(m_bottom_right.y() >= m_top_left.y());
    }

    box_t(index_t const left, index_t const top,
          index_t const right, index_t const bottom)
        : box_t { { left, top }, { right, bottom } }
    {}

    index_t left() const { return m_top_left.x(); }
    index_t right() const { return m_bottom_right.x(); }
    index_t top() const { return m_top_left.y(); }
    index_t bottom() const { return m_bottom_right.y(); }

    point_t const& top_left() const { return m_top_left; }
    point_t const& bottom_right() const { return m_bottom_right; }

    index_t width() const { return right() - left(); }
    index_t height() const { return bottom() - top(); }
    index_t size() const { return width() + height(); }
    index_t delta() const { return width() - height(); }

    index_t k_to_c(index_t const k) const { return k - delta(); }
    index_t c_to_k(index_t const c) const { return c + delta(); }

    index_t to_x(index_t const y, index_t const k) const
    {
        return left() + y - top() + k;
    }

    index_t to_y(index_t const x, index_t const k) const
    {
        return top() + x - left() - k;
    }

    bool contains(index_t const x, index_t const y) const
    {
        return left() <= x && x <= right()
            && top() <= y && y <= bottom();
    }

    bool has_odd_delta() const { return delta() % 2; }
    bool has_even_delta() const { return !has_odd_delta(); }

    friend std::ostream& operator<<(std::ostream& os, box_t const box)
    {
        os << box.m_top_left << ", " << box.m_bottom_right;
        return os;
    }

private:
    point_t m_top_left;
    point_t m_bottom_right;
};

} // namespace stream9::myers_diff

#endif // STREAM9_MYERS_DIFF_BOX_HPP
