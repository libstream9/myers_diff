#ifndef STREAM9_MYERS_DIFF_POINT_HPP
#define STREAM9_MYERS_DIFF_POINT_HPP

#include "index.hpp"

#include <cassert>
#include <cstdint>
#include <iostream>
#include <tuple>

namespace stream9::myers_diff {

class point_t
{
public:
    point_t(index_t const x, index_t const y)
        : m_x { x }
        , m_y { y }
    {
        assert(m_x >= 0);
        assert(m_y >= 0);
    }

    index_t x() const { return m_x; }
    index_t y() const { return m_y; }

    friend std::ostream& operator<<(std::ostream& os, point_t const p)
    {
        os << "[" << p.m_x << ", " << p.m_y << "]";
        return os;
    }

private:
    index_t m_x;
    index_t m_y;
};

template<size_t N>
index_t get(point_t const& point)
{
    if      constexpr (N == 0) return point.x();
    else if constexpr (N == 1) return point.y();
}

} // namespace stream9::myers_diff

namespace std {

    template<>
    struct tuple_size<stream9::myers_diff::point_t> : integral_constant<size_t, 2> {};

    template<size_t N>
    struct tuple_element<N, stream9::myers_diff::point_t>
    {
        using type = stream9::myers_diff::index_t;
    };

} // namespace std

#endif // STREAM9_MYERS_DIFF_POINT_HPP
