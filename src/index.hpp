#ifndef STREAM9_MYERS_DIFF_INDEX_HPP
#define STREAM9_MYERS_DIFF_INDEX_HPP

#include <cstdint>

#include <stream9/safe_integer.hpp>

namespace stream9::myers_diff {

using index_t = stream9::safe_integer<std::intmax_t>;

} // namespace stream9::myers_diff

#endif // STREAM9_MYERS_DIFF_INDEX_HPP
