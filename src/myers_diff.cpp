#include <stream9/myers_diff.hpp>

#include <stream9/myers_diff/namespace.hpp>

#include "array.hpp"
#include "index.hpp"
#include "point.hpp"
#include "snake.hpp"
#include "box.hpp"

#include <cassert>
#include <optional>
#include <ranges>
#include <utility>
#include <vector>

//#include <iostream>
//#include <boost/format.hpp>

#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/views/zip.hpp>

namespace stream9::myers_diff {

using array_type = array_t<index_t>;

static std::optional<snake_t>
forwards(box_t const& box, equal_fn const& equal,
         array_type& vf, array_type const& vb, index_t const d)
{
    std::optional<snake_t> snake;
    auto snake_score = std::numeric_limits<index_t>::min();
    index_t px = 0, py = 0, x = 0, y = 0;

    for (auto k = -d; k <= d; k += 2) {
        if (k == -d || (k != d && vf[k - 1] < vf[k + 1])) {
            px = x = vf[k + 1];
        }
        else {
            px = vf[k - 1];
            x = px + 1;
        }

        y = box.to_y(x, k);
        py = (d == 0 || x != px) ? y : y - 1;

        while (box.contains(x+1, y+1) && equal(x, y)) {
            ++x, ++y;
        }

        vf[k] = x;

        auto const c = box.k_to_c(k);
#if 0
        std::cout << "forward : " << box
                  << ": (d, k, c) = (" << d << ", " << k << ", " << c << ")"
                  << ", (x, y) = (" << x << ", " << y << ")"
                  << ", (px, py) = (" << px << ", " << py << ")"
                  << "\n";
#endif
        if (box.has_odd_delta() &&
                (-(d - 1) <= c && c <= (d - 1)) && y >= vb[c])
        {
            assert(box.contains(x, y));
            assert(box.contains(px, py));

            auto const score =
                    (x - box.left()) + (y - box.top()) - std::abs(k.value());
#if 0
            std::cout << "mid snake candidate: "
                      << point_t { px, py } << " -> " << point_t { x, y }
                      << ", score = " << score << "\n";
#endif
            if (!snake || snake_score < score) {
                snake.emplace(px, py, x, y);
                snake_score = score;
            }
        }
    }

    return snake;
}

static std::optional<snake_t>
backward(box_t const& box, equal_fn const& equal,
         array_type const& vf, array_type& vb, index_t const d)
{
    std::optional<snake_t> snake;
    auto snake_score = std::numeric_limits<index_t>::min();
    index_t px = 0, py = 0, x = 0, y = 0;

    for (auto c = -d; c <= d; c += 2) {
        if (c == -d || (c != d && vb[c - 1] > vb[c + 1])) {
            py = y = vb[c + 1];
        }
        else {
            py = vb[c - 1];
            y = py - 1;
        }

        auto const k = box.c_to_k(c);

        x = box.to_x(y, k);
        px = (d == 0 || y != py) ? x : x + 1;

        while (box.contains(x - 1, y - 1) && equal(x - 1, y - 1)) {
            --x, --y;
        }
#if 0
        std::cout << "backward: " << box
                  << ": (d, k, c) = (" << d << ", " << k << ", " << c << ")"
                  << ", (x, y) = (" << x << ", " << y << ")"
                  << ", (px, py) = (" << px << ", " << py << ")"
                  << "\n";
#endif
        vb[c] = y;

        if (box.has_even_delta() && -d <= k && k <= d && x <= vf[k])
        {
            assert(box.contains(x, y));
            assert(box.contains(px, py));

            auto const score =
                (box.right() - x) + (box.bottom() - y) - std::abs(c.value());
#if 0
            std::cout << "mid snake candidate: "
                      << point_t { x, y } << " <- " << point_t { px, py }
                      << ", score = " << score << ", c = " << c << "\n";
#endif
            if (!snake || snake_score < score) {
                snake.emplace(x, y, px, py);
                snake_score = score;
            }
        }
    }

    return snake;
}

static std::optional<snake_t>
midpoint(box_t const& box, equal_fn const& equal)
{
    if (box.size() == 0) return {};

    index_t const max = static_cast<index_t::value_type>(
                            std::ceil(static_cast<double>(box.size()) / 2.0));

    array_type vf { -max, max };
    vf[1] = box.left();

    array_type vb { -max, max };
    vb[1] = box.bottom();

    for (auto d = 0; d <= max; ++d) {
        auto const snake_f = forwards(box, equal, vf, vb, d);
        auto const snake_b = backward(box, equal, vf, vb, d);

        assert(!(snake_f && snake_b));

        if (snake_f || snake_b) {
            return snake_f ? snake_f : snake_b;
        }
    }

    //std::cout << "no midpoint:" << box << ", max = " << max << "\n";
    return {};
}

static bool
find_path(box_t const& box, equal_fn const& equal,
          std::vector<point_t>& result, int const level = 0)
{
    auto const snake = midpoint(box, equal);

    if (!snake) return false;

    //static boost::format fmt { "%4% - %1%: box: %2%, snake: %3%\n" };

    //std::cout << fmt % "head" % box % *snake % level;
    if (!find_path({ box.top_left(), snake->from() }, equal, result, level+1)) {
        //std::cout << "pushing snake from " << snake->from() << "\n";
        result.push_back(snake->from());
    }

    //std::cout << fmt % "tail" % box % *snake % level;
    if (!find_path({ snake->to(), box.bottom_right() }, equal, result, level+1)) {
        //std::cout << "pushing snake to " << snake->to() << "\n";
        result.push_back(snake->to());
    }

    return true;
}

static std::vector<point_t>
find_path(box_t const& box, equal_fn const& equal)
{
    std::vector<point_t> result;

    find_path(box, equal, result);

    return result;
}

template<rng::random_access_range Range>
auto
adjacent_pairs(Range&& range)
{
    assert(std::size(range) >= 2);

    auto const begin = std::begin(range);
    auto const end = std::end(range);

    auto from = std::ranges::subrange(begin, end-1);
    auto to = std::ranges::subrange(begin+1, end);

    return rng::views::zip(from, to);
}

static point_t
walk_diagonal(point_t const& from, point_t const& to,
              equal_fn const& equal, event_handler& h)
{
    auto [x, y] = from;

    while (x < to.x() && y < to.y() && equal(x, y)) {
        h.unchanged(x, y);

        ++x, ++y;
    }

    return { x, y };
}

static void
walk_path(std::vector<point_t> const& path,
          equal_fn const& equal, event_handler& h)
{
    for (auto const& [from, to]: adjacent_pairs(path)) {

        auto [x, y] = walk_diagonal(from, to, equal, h);

        auto const x_diff = to.x() - x;
        auto const y_diff = to.y() - y;

        if (x_diff > y_diff) {
            h.deleted(x);
            ++x;
        }
        else if (x_diff < y_diff) {
            h.inserted(y);
            ++y;
        }

        walk_diagonal({x, y}, to, equal, h);
    }
}

void
diff(uindex_t const size1, uindex_t const size2,
     equal_fn const& equal, event_handler& h)
{
    if (size1 == 0 && size2 == 0) return;

    assert(std::numeric_limits<index_t>::max() - size1 >= size2);

    auto const path = find_path({ 0, 0, size1, size2 }, equal);

    walk_path(path, equal, h);
}

} // namespace stream9::myers_diff
