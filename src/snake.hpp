#ifndef STREAM9_MYERS_DIFF_SNAKE_HPP
#define STREAM9_MYERS_DIFF_SNAKE_HPP

#include "index.hpp"
#include "point.hpp"

#include <cassert>
#include <iostream>

namespace stream9::myers_diff {

class snake_t
{
public:
    snake_t(point_t const from, point_t const to)
        : m_from { from }
        , m_to { to }
    {
        assert(m_to.x() >= m_from.x());
        assert(m_to.y() >= m_from.y());
    }

    snake_t(index_t const from_x, index_t const from_y,
            index_t const to_x, index_t const to_y)
        : snake_t { { from_x, from_y }, { to_x, to_y } }
    {}

    point_t from() const { return m_from; }
    point_t to() const { return m_to; }

    friend std::ostream& operator<<(std::ostream& os, snake_t const snake)
    {
        os << snake.m_from << ", " << snake.m_to;
        return os;
    }

private:
    point_t m_from;
    point_t m_to;
};

} // namespace stream9::myers_diff

#endif // STREAM9_MYERS_DIFF_SNAKE_HPP
