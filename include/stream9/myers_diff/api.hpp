#ifndef STREAM9_MYERS_DIFF_API_HPP
#define STREAM9_MYERS_DIFF_API_HPP

#include "namespace.hpp"

#include <cstdint>
#include <string_view>

#include <stream9/function_ref.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/size.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::myers_diff {

// Types

using uindex_t = stream9::safe_integer<std::intmax_t, 0>;

// @return true if before[x] == after[y]
using equal_fn = stream9::function_ref<bool(uindex_t/*x*/, uindex_t/*y*/)>;

class event_handler
{
public:
    using uindex_t = myers_diff::uindex_t;

public:
    virtual ~event_handler() = default;

    virtual void unchanged(uindex_t/*x*/, uindex_t/*y*/) {}
    virtual void inserted(uindex_t/*y*/) {}
    virtual void deleted(uindex_t/*x*/) {}
};

// Functions

// Calculate minimum edit sequence between two lists
//
// It is recommended to use convenient functions defined below.
//
// precondition: size1 + size2 <= std::numeric_limits<std::intmax_t>::max()
//
// @param size1 size of first list
// @param size2 size of second list
void diff(uindex_t size1, uindex_t size2,
          equal_fn const&, event_handler&);

// Calculate difference from first string to second string
inline void
diff(std::string_view const first,
     std::string_view const second, event_handler& v)
{
    diff(first.size(), second.size(),
        [&](auto x, auto y) { return first[x] == second[y]; },
        v
    );
}

// Calculate difference from first range to second range
template<rng::random_access_range R1,
         rng::random_access_range R2 >
void
diff(R1 const& first, R2 const& second, event_handler& v)
{
    diff(rng::size(first), rng::size(second),
        [&](auto x, auto y) { return first[x] == second[y]; },
        v
    );
}

} // namespace stream9::myers_diff

#endif // STREAM9_MYERS_DIFF_API_HPP
