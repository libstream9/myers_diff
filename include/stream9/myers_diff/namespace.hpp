#ifndef STREAM9_MYERS_DIFF_NAMESPACE_HPP
#define STREAM9_MYERS_DIFF_NAMESPACE_HPP

namespace stream9::ranges {}

namespace stream9::myers_diff {

namespace rng = stream9::ranges;

} // namespace stream9::myers_diff

#endif // STREAM9_MYERS_DIFF_NAMESPACE_HPP
